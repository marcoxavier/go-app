package worker

import (
	"sync"
	"time"

	"gitlab.com/mandalore/go-app/app"
	log "gitlab.com/vredens/go-logger"
	"gitlab.com/vredens/go-structs"
)

// WorkHandler is the function signature for any function capable of handling a task.
// Both the task ID and task data are provided to the handler. The task ID can and
// should be used to identify task types by the way of a prefix or bit mask.
type WorkHandler func(id string, data interface{}) error

// Option is the abstract functional-parameter type used for worker configuration.
type Option func(*Worker)

// Worker is a generic implementation of a task worker providing processing rate limits and task process delay. Tasks are indexed by an ID so the same task is not performed twice.
type Worker struct {
	queue        *structs.Queue
	mux          *sync.Mutex
	maxPerSecond int
	queueSize    int
	tick         time.Duration
	processDelay time.Duration
	control      chan bool
	started      bool
	handler      WorkHandler
	log          log.Logger
}

// NewWorker creates a new Worker with the provided options.
func NewWorker(opts ...Option) *Worker {
	worker := &Worker{
		maxPerSecond: 0,
		tick:         time.Second,
		queueSize:    10000,
		processDelay: time.Second,
		control:      make(chan bool),
		started:      false,
		mux:          &sync.Mutex{},
		handler:      nil,
		log:          log.Spawn(log.WithTags("worker")),
	}

	for _, opt := range opts {
		opt(worker)
	}

	worker.queue = structs.NewQueue(worker.queueSize)

	return worker
}

// SetHandler allows setting a work handler even after starting the worker.
func (w *Worker) SetHandler(f WorkHandler) {
	w.mux.Lock()
	w.handler = f
	w.mux.Unlock()
}

// Start the worker.
func (w *Worker) Start() error {
	w.mux.Lock()
	if w.started {
		w.mux.Unlock()
		return app.NewError(app.ErrorDevPoo, "worker is already running", nil)
	}
	if w.handler == nil {
		w.mux.Unlock()
		return app.NewError(app.ErrorDevPoo, "no task handler was set", nil)
	}
	w.started = true
	w.mux.Unlock()

	// TODO: use golang.org/x/time/rate Limiter.WaitN instead, combines max per second with tick

	for {
		select {
		case <-time.After(w.tick):
			if w.queue.Size() > 0 {
				size := w.queue.Size()
				w.log.Debugf("process batch starting [size:%d]", w.queue.Size())
				w.processBatch()
				w.log.Debugf("process batch done [size:%d]", w.queue.Size())
				app.StatsAverageAdd("worker-process-batch", float64(size-w.queue.Size()))
			}
		case <-w.control:
			return nil
		}
	}
}

// Stop ...
func (w *Worker) Stop() error {
	w.mux.Lock()
	if !w.started {
		w.mux.Unlock()
		return app.NewError(app.ErrorConcurrencyException, "worker is already stopped", nil)
	}
	// TODO: race condition here, need to use a w.state instead and use a 'started', 'stopping' and 'stopped' states
	w.mux.Unlock()

	w.control <- true

	return nil
}

// Process ...
func (w *Worker) Process(taskID string, data interface{}) error {
	task := NewTask(taskID, data)
	return w.queue.Add(task.id, task)
}

func (w *Worker) processBatch() {
	cont := w.processNextTask()
	cnt := 1
	for cont && (w.maxPerSecond == 0 || float64(cnt) < float64(w.maxPerSecond)*w.tick.Seconds()) {
		cont = w.processNextTask()
		cnt++
	}
}

func (w *Worker) processNextTask() bool {
	var task *Task
	if tmp := w.queue.Pop(); tmp != nil {
		task = tmp.(*Task)
	} else {
		return false
	}

	if w.processDelay > 0 && task.Age() < w.processDelay {
		if err := w.queue.Add(task.id, task); err != nil {
			w.log.Errorf("unexpected error re-adding a task to the queue; %s", app.StringifyError(err))
		}
		return false
	}
	if err := w.handler(task.id, task.data); err != nil {
		w.log.Errorf("task handler failed; %s", app.StringifyError(err))
		return false
	}
	return true
}
